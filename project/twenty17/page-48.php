<?php
/**
 * The template for my Asparagus page
 *
 * This is the template that displaysasparagus by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty17
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

                    <div>
                        <a data-flickr-embed="true" href="https://www.flickr.com/photos/papermonkey/5000873805/in/photolist-8BUMya-4m1m7X-7bb7B7-4kwDVF-7eiJkQ-LKzFU-9Pbmbf-59Rdb8-c7dN-9YPGA9-bcsnwT-dRKe88-8twAnV-paPPwe-a3t6AM-6qqNsd-WyAg6V-TCrm77-FgdZd1-5MCp3x-5NQFT6-28oHwrE-25hcZAc-6DtKGs-5LBYnJ-anXtLn-6fqfRA-4H6C4j-PaNvBa-5dgwit-2ddkRE6-RsMC42-XbXwTt-RVPY9M-KRrQF7-2c7PpvY-Kx117m-aMVKRF-qwggvT-2brNAnd-2cZmYqX-23cSrCN-51rsHx-TLg8No-5WMdJM-2fQHAAP-2cQDDgw-GexPcU-8thd23-bx8k2i" title="thank you"><img src="https://live.staticflickr.com/4152/5000873805_126752a6c8_b.jpg" width="1024" height="683" alt="thank you"></a>
                        <script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
                    </div>

            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <!-- .wrap -->

    <?php
get_footer();