<?php
/**
 * The template for my Asparagus page
 *
 * This is the template that displaysasparagus by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty17
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <div>
                    <style type="text/css">
                        .tg {
                            border-collapse: collapse;
                            border-spacing: 0;
                        }
                        
                        .tg td {
                            font-family: Arial, sans-serif;
                            font-size: 14px;
                            padding: 10px 5px;
                            border-style: solid;
                            border-width: 1px;
                            overflow: hidden;
                            word-break: normal;
                            border-color: black;
                        }
                        
                        .tg th {
                            font-family: Arial, sans-serif;
                            font-size: 14px;
                            font-weight: normal;
                            padding: 10px 5px;
                            border-style: solid;
                            border-width: 1px;
                            overflow: hidden;
                            word-break: normal;
                            border-color: black;
                        }
                        
                        .tg .tg-baqh {
                            text-align: center;
                            vertical-align: top
                        }
                    </style>
                    <table class="tg">
                        <tr>
                            <th class="tg-baqh"></th>
                            <th class="tg-baqh">Contax 645</th>
                            <th class="tg-baqh">Mamiya 7II</th>
                            <th class="tg-baqh">pentax 67</th>
                        </tr>
                        <tr>
                            <td class="tg-baqh">Format</td>
                            <td class="tg-baqh">645</td>
                            <td class="tg-baqh">67</td>
                            <td class="tg-baqh">67</td>
                        </tr>
                        <tr>
                            <td class="tg-baqh">AF</td>
                            <td class="tg-baqh">yes</td>
                            <td class="tg-baqh">no</td>
                            <td class="tg-baqh">no</td>
                        </tr>
                        <tr>
                            <td class="tg-baqh">Price</td>
                            <td class="tg-baqh">$6000</td>
                            <td class="tg-baqh">$3000</td>
                            <td class="tg-baqh">$2500</td>
                        </tr>
                    </table>

                </div>
                <?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <!-- .wrap -->

    <?php
get_footer();