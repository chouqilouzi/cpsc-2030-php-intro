<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Checkout example · Bootstrap</title>
    <!--<link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/checkout/"> -->
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
</head>

<body class="bg-light">
    <div class="container">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="bootstrap-solid.svg" alt="" width="72" height="72">
            <h2>Checkout form</h2>
            <p class="lead">Below is an example form built entirely with Bootstrap’s form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p>
        </div>
        <?php
          echo "<h4>" . "First name : " . $_POST['firstNameI'] . "</h4>"; 
          echo "<h5>" . "Last name : " . $_POST['lastNameI'] . "</h5>";
          echo "<br>";
          echo "<h5>" . "Email address : " . $_POST['emailI'] . "</h5>";
          echo "<h5>" . "address : " . $_POST['address1I'] .  " " . $_POST['address2I'] ."</h5>";
          echo "<h5>" . "country : " . $_POST['countryI'] . "</h5>";
          echo "<h5>" . "Province : " . $_POST['provinceI'] . "</h5>";
          echo "<h5>" . "Postal Code : " . $_POST['postalCodeI'] . "</h5>";
          
          echo "<br>";
          echo "<h5>" . "Name on card : " . $_POST['cc-nameI'] . "</h5>";
          echo "<h5>" . "Credit card number : " . $_POST['cc-numberI'] . "</h5>";
          echo "<h5>" . "Expiration date : " . $_POST['cc-expI'] . "</h5>";
          echo "<h5>" . "CVV : " . $_POST['ccvI'] . "</h5>";
          
          echo "<br>";
          if (isset($_POST['gwI'])){
           echo "<h5>" . "The User choose wrap <br/>" . "</h5>";
}
          
          echo "<br>";
          
          $delivery= $_POST ['delivery'];
          echo "<h5>" . "Delivery : "  . $delivery. "</h5>";
          echo "<br>";
          $price1=$_POST['item_1'] * 12 ;
          $price2=$_POST['item_2'] * 8;
          $price3=$_POST['item_3'] * 5;
          
          echo "<h5>" .  $_POST['item_1'] . "</h5>";
          
          $totalPrice=$price1 + $price2 + $price3;
          echo "<h5>" . "price in total before tax : " . $totalPrice . "</h5>";
       
          echo "<h5>" . "price in total after tax : " . $totalPrice * 1.12 . "</h5>";
          
          
          
          
          
          
          
        ?>
     </div>
</body> 

</html>







